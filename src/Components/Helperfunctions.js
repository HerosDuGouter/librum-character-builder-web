export function FindItem(array,id, property) {
    let result = array.find( object => object.id === id)
    return result[property]
}

export const Reducer = (state, action) => {
    switch (action.type) {
        case 'SET_RACE':
            return {
                ...state,
                raceID: action.payload.raceID,
                raceChosen: true
            };
        case 'ADD_POST':
            return {
                ...state,
                posts: state.posts.concat(action.payload)
            };
        case 'REMOVE_POST':
            return {
                ...state,
                posts: state.posts.filter(post => post.id !== action.payload)
            };
        case 'SET_ERROR':
            return {
                ...state,
                error: action.payload
            };
        default:
            return state;
    }
};

