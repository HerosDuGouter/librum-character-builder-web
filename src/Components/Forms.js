import React,{ useState } from 'react'
import { VStack, Box, Select, Container } from '@chakra-ui/react'
import RaceForm from './Forms/RaceForm'
import RaceCard from './Forms/RaceCard'

export default function Form() {
    return (
    
         <VStack>
            <RaceForm />
            <RaceCard />
         </VStack>
    
    )
}
