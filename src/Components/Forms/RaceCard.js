import React, {useContext} from 'react'
import {Box, Container} from '@chakra-ui/react'
import {FindItem} from '../Helperfunctions'
import {Context} from '../../Store'
import RaceList from './Datas/RaceList.json'

export default function RaceCard() {
    const [state] = useContext(Context)
    return (
        <Container>
        {   state.raceID
            ? <Box>{FindItem(RaceList, state.raceID, "description")}</Box> 
            : <Box>Different races have different abilities. Choose wisely !</Box> 
        }
        </Container>
    )
}
