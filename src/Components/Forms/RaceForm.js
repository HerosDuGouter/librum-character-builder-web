import React,{ useContext } from 'react'
import { Box, Select, Container } from '@chakra-ui/react'
import RaceList from './Datas/RaceList.json'
import {Context} from '../../Store'


export default function RaceForm() {
    const [state, dispatch] = useContext(Context)

    return (
        <Container minWidth="xl" mt="10">
        <div>
            <Box bg="blue.600" w="100%" p={4} color="white">
            RACE
            </Box>
            <Select 
                onChange={ e=> {
                const newRace = { raceID: e.target.value, raceChosen:'true'};
                dispatch({type: 'SET_RACE', payload: newRace});
                console.log(state)
            }} 
                placeholder={
                    state.raceID
                    ? ""
                    : "Choose a race."
                } 
                    >
                {RaceList.map(race => {
                    return(
                        <option value={race.id} key={race.id}>{race.name}</option>
                    )
                })
                }
            </Select>
          
        </div>
        </Container>
    )
}
