import React from 'react'
import {Box,Heading, Flex, Link} from '@chakra-ui/react'
import { ColorModeSwitcher } from '../ColorModeSwitcher';

export default function Navbar() {
    return (
<Flex  
            as="nav"  
            align="center"  
            justify="space-between"  
            wrap="wrap"  
            padding="1.5rem"  
            bg="gray.900"  
            color="white"  
            borderBottom="1px solid black"   
        >  
            <Flex align="center" mr={5}>  
                <Heading as="h1" size="2xl" >  
                    Librum Build Creator  
                </Heading>  
            </Flex>  
            
            <ColorModeSwitcher></ColorModeSwitcher>  
        </Flex>
    )
}
