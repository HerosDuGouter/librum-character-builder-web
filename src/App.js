import React from 'react';
import {
  ChakraProvider,
  Text,
} from '@chakra-ui/react';
import { ColorModeSwitcher } from './ColorModeSwitcher';
import { Logo } from './Logo';
import Navbar from './Components/Navbar';
import Forms from './Components/Forms';
import Store from './Store'


// Theme and Fonts
import theme from './theme'
import '@fontsource/oswald/700.css'

function App() {
  return (
    <Store>
    <ChakraProvider theme={theme}>
      <Navbar />
      <Forms />
    </ChakraProvider>
    </Store>
  );
}

export default App;
