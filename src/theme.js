import { extendTheme } from "@chakra-ui/react"


const theme = extendTheme({
  fonts: {
    heading: "Oswald",
    box: "Oswald",
  },
  config: {
      useSystemColorMode: "true",
  }
})

export default theme